package com.fihtdc.demo.metadata;

public class DataType {

    public class Image {
        public static final String IMAGE = "image/*";
        public static final String IMAGE_JPG = "image/jpg";
        public static final String IMAGE_PNG = "image/png";
        public static final String IMAGE_GIF = "image/gif";
    }

    public class Audio {
        public static final String AUDIO = "audio/midi";
        public static final String AUDIO_MIDI = "audiox-wav";
        public static final String AUDIO_MPEG = "audio/x-mpeg";
        public static final String AUDIO_WAV = "audiox-wav";
    }

    public class Video {
        public static final String VIDEO = "video/*";
        public static final String VIDEO_MP4 = "video/mp4";
        public static final String VIDEO_MPEG = "video/mpeg";
        public static final String VIDEO_3GP = "video/3gpp";
    }

    public class Text {
        public static final String TEXT = "text/*";
        public static final String TEXT_PLAIN = "text/plain";
        public static final String TEXT_HTML = "text/html";
    }

    public class Application {
        public static final String APPLICATION = "application/*";
        public static final String APPLICATION_PDF = "application/pdf";
    }
}
