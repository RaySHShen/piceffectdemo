package com.fihtdc.demo.effect;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fihtdc.demo.utils.BitmapUtil;
import com.fihtdc.demo.utils.FileUtil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Bitmap.Config;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ViewGroup;

public class EffectView extends ViewGroup implements ScaleGestureDetector.OnScaleGestureListener {
    private static final String TAG = "EffectView";

    public static enum Effect {
        GRID, COLOR, BLUR,
    };

    public static enum Mode {
        GRID, PATH,
    }

    // default image inner padding, in dip pixels
    private static final int INNER_PADDING = 6;

    // default grid width, in dip pixels
    private static final int GRID_WIDTH = 5;

    // default grid width, in dip pixels
    private static final int PATH_WIDTH = 20;

    // default stroke rectangle color
    private static final int STROKE_COLOR = 0xff2a5caa;

    // default stroke width, in pixels
    private static final int STROKE_WIDTH = 6;

    private int mImageWidth;
    private int mImageHeight;

    private Bitmap mBaseLayer;
    private Bitmap mCoverLayer;
    private Bitmap mMosaicLayer;

    private Point mStartPoint;

    private int mGridWidth;
    private int mPathWidth;

    private int mStrokeWidth;

    private int mStrokeColor;

    private Effect mEffect;
    private Mode mMode;

    private Rect mImageRect;

    private Paint mPaint;

    private Rect mTouchRect;
    private List<Rect> mTouchRects;

    private Path mTouchPath;
    private List<Rect> mEraseRects;

    private int mMosaicColor;
    private int mPadding;

    private List<Path> mTouchPaths;
    private List<Path> mErasePaths;

    private boolean mMosaic;

    private ScaleGestureDetector mScaleGestureDetector;

    private float mScaleFactor = 1.0f;
    private Rect mInitImageRect;

    private Paint mCirclePaint;

    private int mLastPointerCount = 0;
    private float mLastX;
    private float mLastY;
    private boolean mIsCanDrag;
    private boolean mIsMultiPointer = false;
    private Bitmap mTouchLayer;

    private long mLastCheckDrawTime = 0;
    private boolean mIsCanDrawPath = false;

    public EffectView(Context context) {
        this(context, null);
    }

    public EffectView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initImage();
        mScaleGestureDetector = new ScaleGestureDetector(context,this);
        mCirclePaint = new Paint();
        mCirclePaint.setAntiAlias(true);
        mCirclePaint.setColor(Color.WHITE);
        mCirclePaint.setStrokeWidth(5.0f);
        mCirclePaint.setStyle(Paint.Style.STROKE);
    }

    private void initImage() {
        mMosaic = true;

        mTouchRects = new ArrayList<Rect>();
        mEraseRects = new ArrayList<Rect>();

        mTouchPaths = new ArrayList<Path>();
        mErasePaths = new ArrayList<Path>();

        mStrokeWidth = STROKE_WIDTH;
        mStrokeColor = STROKE_COLOR;

        mPadding = dp2px(INNER_PADDING);

        mPathWidth = dp2px(PATH_WIDTH);
        mGridWidth = dp2px(GRID_WIDTH);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(mStrokeWidth);
        mPaint.setColor(mStrokeColor);

        mImageRect = new Rect();
        mInitImageRect = new Rect();
        setWillNotDraw(false);

        mMode = Mode.PATH;
        mEffect = Effect.GRID;
    }

    public void setImagePath(String filePath, int reqWidth, int reqHeight) {
        File file = new File(filePath);
        if (file == null || !file.exists()) {
            Log.w(TAG, "invalid file path " + filePath);
            return;
        }

        reset();

        Bitmap bitmap = BitmapUtil.decodeBitmap(filePath, reqWidth, reqHeight);
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        Log.v(TAG, "bitmapWidth : " + bitmapWidth + " ,bitmapHeight : " + bitmapHeight);
        /*float scaleWidth = 1;
        float scaleHeight = 1;
        if(bitmapWidth >= reqWidth || bitmapHeight >= reqHeight) {
            int targetWidth;
            int targetHeight;
            if(bitmapWidth >= bitmapHeight) {
                targetWidth = reqWidth;
                targetHeight = reqWidth * bitmapHeight / bitmapWidth;
            } else {
                targetHeight = reqHeight;
                targetWidth = reqHeight * bitmapWidth / bitmapHeight;
            }

            scaleWidth = (float) targetWidth / bitmapWidth;
            scaleHeight = (float) targetHeight / bitmapHeight;
        }*/

        float scaleWidth = (float) reqWidth / bitmapWidth;
        float scaleHeight = (float) reqHeight / bitmapHeight;

        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        mBaseLayer = Bitmap.createBitmap(bitmap, 0, 0, bitmapWidth, bitmapHeight, matrix, true);

        mImageWidth = mBaseLayer.getWidth();
        mImageHeight = mBaseLayer.getHeight();
        mCoverLayer = getCoverLayer();
        mMosaicLayer = null;

        requestLayout();
        invalidate();
    }

    public void setEffect(Effect effect) {
        if (mEffect == effect) {
            Log.d(TAG, "duplicated effect " + effect);
            return;
        }

        this.mEffect = effect;
        if (mCoverLayer != null) {
            mCoverLayer.recycle();
        }

        mCoverLayer = getCoverLayer();
        if (mMode == Mode.GRID) {
            updateGridMosaic();
        } else if (mMode == Mode.PATH) {
            updatePathMosaic();
        }

        invalidate();
    }

    public void setMode(Mode mode) {
        if (mMode == mode) {
            Log.d(TAG, "duplicated mode " + mode);
            return;
        }

        if (mMosaicLayer != null) {
            mMosaicLayer.recycle();
            mMosaicLayer = null;
        }

        this.mMode = mode;

        invalidate();
    }

    private Bitmap getCoverLayer() {
        Bitmap bitmap = null;
        if (mEffect == Effect.GRID) {
            bitmap = Effects.getGridMosaic(mBaseLayer, mImageWidth, mImageHeight, mGridWidth);
        } else if (mEffect == Effect.COLOR) {
            bitmap = Effects.getColorMosaic(mImageWidth, mImageHeight, mMosaicColor);
        } else if (mEffect == Effect.BLUR) {
            bitmap = Effects.getBlurMosaic(mBaseLayer, mImageWidth, mImageHeight);
        }
        return bitmap;
    }

    public boolean isSaved() {
        return (mCoverLayer == null);
    }

    public void setGridWidth(int width) {
        this.mGridWidth = dp2px(width);
    }

    public void setPathWidth(int width) {
        this.mPathWidth = dp2px(width);
    }

    public int getGridWidth() {
        return this.mGridWidth;
    }

    public void setStrokeColor(int color) {
        this.mStrokeColor = color;
        mPaint.setColor(mStrokeColor);
    }

    public void setMosaicColor(int color) {
        this.mMosaicColor = color;
    }

    public int getStrokeColor() {
        return this.mStrokeColor;
    }

    public void setStrokeWidth(int width) {
        this.mStrokeWidth = width;
        mPaint.setStrokeWidth(mStrokeWidth);
    }

    public int getStrokeWidth() {
        return this.mStrokeWidth;
    }

    public void setErase(boolean erase) {
        this.mMosaic = !erase;
    }

    public void clear() {
        mTouchRects.clear();
        mEraseRects.clear();

        mTouchPaths.clear();
        mErasePaths.clear();

        if (mMosaicLayer != null) {
            mMosaicLayer.recycle();
            mMosaicLayer = null;
        }

        if (mTouchLayer != null){
            mTouchLayer.recycle();
            mTouchLayer = null;
        }

        invalidate();
    }

    public boolean reset() {
        if (mCoverLayer != null) {
            mCoverLayer.recycle();
            mCoverLayer = null;
        }
        if (mBaseLayer != null) {
            mBaseLayer.recycle();
            mBaseLayer = null;
        }
        if (mMosaicLayer != null) {
            mMosaicLayer.recycle();
            mMosaicLayer = null;
        }
        if (mTouchLayer != null){
            mTouchLayer.recycle();
            mTouchLayer = null;
        }

        mTouchRects.clear();
        mEraseRects.clear();

        mTouchPaths.clear();
        mErasePaths.clear();
        return true;
    }

    public boolean save(Context context) {
        if (mMosaicLayer == null) {
            return false;
        }

        Bitmap bitmap = Bitmap.createBitmap(mImageWidth, mImageHeight, Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawBitmap(mBaseLayer, 0, 0, null);
        canvas.drawBitmap(mMosaicLayer, 0, 0, null);
        canvas.save();

        FileOutputStream fos = null;
        File outputFile = null;
        try {
            String fileName = "Mosaic_" + System.currentTimeMillis() + ".jpg";
            File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            outputFile = new File(file, fileName);
            fos = new FileOutputStream(outputFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "failed to write image content");
            return false;
        } finally {
            try {
                if(fos != null) {
                    fos.flush();
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileUtil.notifyMediaScanner(context, outputFile);
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mScaleGestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    public boolean dispatchTouchEvent(MotionEvent event) {
        super.dispatchTouchEvent(event);

        float pointerX = 0,pointerY = 0;

        int pointerCount = event.getPointerCount();

        for (int i = 0; i < pointerCount; i++){
            pointerX += event.getX(i);
            pointerY += event.getY(i);
        }
        pointerX = pointerX / pointerCount;
        pointerY = pointerY / pointerCount;
        if (pointerCount > 1){
            mIsMultiPointer = true;

            mIsCanDrawPath = false;
            mLastCheckDrawTime = 0;
        }
        if (mLastPointerCount != pointerCount){
            mLastX = pointerX;
            mLastY = pointerY;
            mIsCanDrag = false;
            mLastPointerCount = pointerCount;
        }

        if (mIsMultiPointer){
            switch (event.getAction()){
                case MotionEvent.ACTION_MOVE:
                    if (pointerCount == 1)
                        break;
                    if (mImageRect.width() > mInitImageRect.width()) {
                        int dx = (int) (pointerX - mLastX);
                        int dy = (int) (pointerY - mLastY);
                        if (!mIsCanDrag)
                            mIsCanDrag = isCanDrag(dx,dy);
                        if (mIsCanDrag) {
                            if (mImageRect.left + dx > mInitImageRect.left)
                                dx = mInitImageRect.left - mImageRect.left;
                            if (mImageRect.right + dx < mInitImageRect.right)
                                dx = mInitImageRect.right - mImageRect.right;
                            if (mImageRect.top + dy > mInitImageRect.top)
                                dy = mInitImageRect.top - mImageRect.top;
                            if (mImageRect.bottom + dy < mInitImageRect.bottom)
                                dy = mInitImageRect.bottom - mImageRect.bottom;
                            mImageRect.offset(dx, dy);
                        }
                    }
                    mLastX = pointerX;
                    mLastY = pointerY;
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    mLastPointerCount = 0;
                    mIsMultiPointer = false;
                    break;
            }
            return true;
        }
        int action = event.getAction();
        int x = (int) event.getX();
        int y = (int) event.getY();

        if (!mIsCanDrawPath){
            if(mLastCheckDrawTime == 0){
                mLastCheckDrawTime = System.currentTimeMillis();
            }
            if (System.currentTimeMillis() - mLastCheckDrawTime > 50) {
                mIsCanDrawPath = true;
            }
        }

        if (mMode == Mode.GRID) {
            onGridEvent(action, x, y);
        } else if (mMode == Mode.PATH) {
            onPathEvent(action, x, y);
        }

        return true;
    }

    private boolean isCanDrag(int dx, int dy) {
        return Math.sqrt((dx * dx) + (dy * dy)) >= 5.0f;
    }

    private void onGridEvent(int action, int x, int y) {
        if (x >= mImageRect.left && x <= mImageRect.right
                && y >= mImageRect.top && y <= mImageRect.bottom) {
            int left = x;
            int right = x;
            int top = y;
            int bottom = y;
            if (mStartPoint == null) {
                mStartPoint = new Point();
                mStartPoint.set(x, y);
                mTouchRect = new Rect();
            } else {
                left = mStartPoint.x < x ? mStartPoint.x : x;
                top = mStartPoint.y < y ? mStartPoint.y : y;
                right = x > mStartPoint.x ? x : mStartPoint.x;
                bottom = y > mStartPoint.y ? y : mStartPoint.y;
            }
            mTouchRect.set(left, top, right, bottom);
        }

        if (action == MotionEvent.ACTION_UP) {
            if (mMosaic) {
                mTouchRects.add(mTouchRect);
            } else {
                mEraseRects.add(mTouchRect);
            }
            mTouchRect = null;
            mStartPoint = null;
            updateGridMosaic();
        }

        invalidate();
    }

    private void onPathEvent(int action, int x, int y) {

        if (mImageWidth <= 0 || mImageHeight <= 0) {
            return;
        }

        if (x < mImageRect.left || x > mImageRect.right || y < mImageRect.top
                || y > mImageRect.bottom) {
            return;
        }

        float ratio = (mImageRect.right - mImageRect.left) / (float) mImageWidth;
        x = (int) (((x - mImageRect.left) / ratio));
        y = (int) (((y - mImageRect.top) / ratio));

        if (action == MotionEvent.ACTION_DOWN) {
            mTouchPath = new Path();
            mTouchPath.moveTo(x, y);
            if (mMosaic) {
                mTouchPaths.add(mTouchPath);
            } else {
                mErasePaths.add(mTouchPath);
            }
        } else if (action == MotionEvent.ACTION_MOVE) {
            if (mIsCanDrawPath) {
                mTouchPath.lineTo(x, y);
                updatePathMosaic();
                invalidate();
            }
        }else if (action == MotionEvent.ACTION_UP){
            mIsCanDrawPath = false;
            mLastCheckDrawTime = 0;
        }
    }

    private void updatePathMosaic() {
        if (mImageWidth <= 0 || mImageHeight <= 0) {
            return;
        }

        if (mMosaicLayer == null)
            mMosaicLayer = Bitmap.createBitmap(mImageWidth, mImageHeight, Config.ARGB_4444);
        if (mTouchLayer == null){
            mTouchLayer = Bitmap.createBitmap(mImageWidth, mImageHeight, Config.ARGB_4444);
        }

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setPathEffect(new CornerPathEffect(10));
        paint.setStrokeWidth(mPathWidth);
        paint.setColor(Color.BLUE);

        Canvas canvas = new Canvas(mTouchLayer);

        for (Path path : mTouchPaths) {
            canvas.drawPath(path, paint);
        }

        paint.setColor(Color.TRANSPARENT);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        for (Path path : mErasePaths) {
            canvas.drawPath(path, paint);
        }

        canvas.setBitmap(mMosaicLayer);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawBitmap(mCoverLayer, 0, 0, null);

        paint.reset();
        paint.setAntiAlias(true);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawBitmap(mTouchLayer, 0, 0, paint);
        paint.setXfermode(null);
        canvas.save();
    }

    private void updateGridMosaic() {
        if (mImageWidth <= 0 || mImageHeight <= 0) {
            return;
        }

        if (mMosaicLayer != null) {
            mMosaicLayer.recycle();
        }
        mMosaicLayer = Bitmap.createBitmap(mImageWidth, mImageHeight, Config.ARGB_8888);

        float ratio = (mImageRect.right - mImageRect.left) / (float) mImageWidth;
        Bitmap bmTouchLayer = Bitmap.createBitmap(mImageWidth, mImageHeight, Config.ARGB_8888);

        Canvas canvas = null;
        canvas = new Canvas(bmTouchLayer);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(mStrokeColor);

        for (Rect rect : mTouchRects) {
            int left = (int) ((rect.left - mImageRect.left) / ratio);
            int right = (int) ((rect.right - mImageRect.left) / ratio);
            int top = (int) ((rect.top - mImageRect.top) / ratio);
            int bottom = (int) ((rect.bottom - mImageRect.top) / ratio);
            canvas.drawRect(left, top, right, bottom, paint);
        }

        paint.setColor(Color.TRANSPARENT);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        for (Rect rect : mEraseRects) {
            int left = (int) ((rect.left - mImageRect.left) / ratio);
            int right = (int) ((rect.right - mImageRect.left) / ratio);
            int top = (int) ((rect.top - mImageRect.top) / ratio);
            int bottom = (int) ((rect.bottom - mImageRect.top) / ratio);
            canvas.drawRect(left, top, right, bottom, paint);
        }

        canvas.setBitmap(mMosaicLayer);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawBitmap(mCoverLayer, 0, 0, null);

        paint.reset();
        paint.setAntiAlias(true);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawBitmap(bmTouchLayer, 0, 0, paint);
        paint.setXfermode(null);
        canvas.save();

        bmTouchLayer.recycle();
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mBaseLayer != null) {
            //canvas.scale(scaleFactor, scaleFactor, mImageRect.centerX(), mImageRect.centerY());
            canvas.drawBitmap(mBaseLayer, null, mImageRect, null);
        }

        if (mMosaicLayer != null) {
            canvas.drawBitmap(mMosaicLayer, null, mImageRect, null);
        }
        if (mTouchRect != null) {
            canvas.drawRect(mTouchRect, mPaint);
        }
    }

    @Override
    public boolean onScale(ScaleGestureDetector detector) {
        float scale = detector.getScaleFactor();
        mScaleFactor *= scale;
        if (mScaleFactor < 1.0f) {
            mScaleFactor = 1.0f;
        }

        if (mScaleFactor > 2.0f) {
            mScaleFactor = 2.0f;
        }

        if (mImageRect != null){
            int addWidth =(int) (mInitImageRect.width() * mScaleFactor) - mImageRect.width ();
            int addHeight=(int) (mInitImageRect.height() * mScaleFactor) - mImageRect.height();
            float centerWidthRatio = (detector.getFocusX() - mImageRect.left) / mImageRect.width();
            float centerHeightRatio = (detector.getFocusY() - mImageRect.left) / mImageRect.height();

            int leftAdd = (int) (addWidth * centerWidthRatio);
            int topAdd = (int) (addHeight * centerHeightRatio);

            mImageRect.left =  mImageRect.left - leftAdd;
            mImageRect.right = mImageRect.right + (addWidth - leftAdd);
            mImageRect.top = mImageRect.top - topAdd;
            mImageRect.bottom = mImageRect.bottom + (addHeight - topAdd);
            checkCenterWhenScale();
        }

        invalidate();
        return true;
    }

    private void checkCenterWhenScale() {
        int deltaX = 0;
        int deltaY = 0;
        if (mImageRect.left > mInitImageRect.left){
            //mImageRect.offsetTo(mInitImageRect.left, mImageRect.top);
            deltaX = mInitImageRect.left - mImageRect.left;
        }
        if (mImageRect.right < mInitImageRect.right){
            deltaX = mInitImageRect.right - mImageRect.right;
        }
        if (mImageRect.top > mInitImageRect.top){
            deltaY = mInitImageRect.top - mImageRect.top;
        }
        if (mImageRect.bottom < mInitImageRect.bottom){
            deltaY = mInitImageRect.bottom - mImageRect.bottom;
        }
        mImageRect.offset(deltaX,deltaY);
    }

    @Override
    public boolean onScaleBegin(ScaleGestureDetector detector) {
        return true;
    }

    @Override
    public void onScaleEnd(ScaleGestureDetector detector) {

    }

    protected void onLayout(boolean changed, int left, int top, int right,
            int bottom) {
        if (mImageWidth <= 0 || mImageHeight <= 0) {
            return;
        }

        int contentWidth = right - left;
        int contentHeight = bottom - top;
        int viewWidth = contentWidth - mPadding * 2;
        int viewHeight = contentHeight - mPadding * 2;
        float widthRatio = viewWidth / ((float) mImageWidth);
        float heightRatio = viewHeight / ((float) mImageHeight);
        float ratio = widthRatio < heightRatio ? widthRatio : heightRatio;
        int realWidth = (int) (mImageWidth * ratio);
        int realHeight = (int) (mImageHeight * ratio);

        int imageLeft = (contentWidth - realWidth) / 2;
        int imageTop = (contentHeight - realHeight) / 2;
        int imageRight = imageLeft + realWidth;
        int imageBottom = imageTop + realHeight;
        mImageRect.set(imageLeft, imageTop, imageRight, imageBottom);
        mInitImageRect.set(imageLeft,imageTop,imageRight,imageBottom);
    }

    private int dp2px(int dip) {
        Context context = this.getContext();
        Resources resources = context.getResources();
        int px = Math
                .round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                        dip, resources.getDisplayMetrics()));
        return px;
    }
}
