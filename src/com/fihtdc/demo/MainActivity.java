package com.fihtdc.demo;

import java.io.File;

import com.fihtdc.demo.effect.EffectView;
import com.fihtdc.demo.effect.EffectView.Effect;
import com.fihtdc.demo.effect.EffectView.Mode;
import com.fihtdc.demo.metadata.DataType;
import com.fihtdc.demo.utils.FileUtil;
import com.fihtdc.demo.utils.PermissionUtil;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Toast;

public class MainActivity extends Activity {

    private String TAG = "MainActivity";

    private Button mBtnOpenCamera;
    private Button mBtnOpenPhoto;
    private Button mBtnMode;
    private Button mBtnEffect;

    private EffectView mEffectView;
    private Uri mCameraUri;

    private Context mContext;

    private static final int REQ_PICK_IMAGE = 1;
    private static final int TAKE_PHOTO = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        setView();
        setListener();
        if (!PermissionUtil.isAllPermissionGranted(mContext)) {
            PermissionUtil.showPermissionDialog(mContext, MainActivity.this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_save:
                if(mEffectView.save(mContext)) {
                    Toast.makeText(mContext, R.string.tips_file_save, Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.action_clear:
                mEffectView.clear();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void setView() {
        mEffectView = (EffectView) findViewById(R.id.img);
        mBtnOpenCamera = (Button) findViewById(R.id.btn_open_camere);
        mBtnOpenPhoto = (Button) findViewById(R.id.btn_open_photo);
        mBtnMode = (Button) findViewById(R.id.btn_mode);
        mBtnEffect = (Button) findViewById(R.id.btn_effect);
    }

    private void setListener() {
        Button buttons[] = {mBtnOpenCamera, mBtnOpenPhoto, mBtnMode, mBtnEffect};
        for(Button button : buttons) {
            button.setOnClickListener(mBtnListener);
        }
    }

    private void openCamera() {
        String fileName = "Recipe_" + System.currentTimeMillis() + ".jpg";
        File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File outputFile = new File(file, fileName);
        FileUtil.createFile(outputFile);

        // notify media scanner
        FileUtil.notifyMediaScanner(mContext, outputFile);

        // launch camera application
        mCameraUri = Uri.fromFile(outputFile);
        Intent cameraIntent = new Intent();
        cameraIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCameraUri);
        startActivityForResult(cameraIntent, TAKE_PHOTO);
    }

    private void choosePick() {
        // filter photo application
        Intent photoIntnet = new Intent();
        photoIntnet.setAction(Intent.ACTION_PICK);
        photoIntnet.setType(DataType.Image.IMAGE);
        String tips = getResources().getString(R.string.tips_choose_image);
        Intent chooserIntent = Intent.createChooser(photoIntnet, tips);
        startActivityForResult(chooserIntent, REQ_PICK_IMAGE);
    }

    private String getFilePath(Intent data) {
        Cursor cursor = null;
        String filePath = null;
        try {
            Uri uri = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
            if(cursor == null) return null;
            if(cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                filePath = cursor.getString(columnIndex);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return filePath;
    }

    private Button.OnClickListener mBtnListener = new Button.OnClickListener() {

        @Override
        public void onClick(View v) {
            switch(v.getId()) {
                case R.id.btn_open_camere:
                    openCamera();
                    break;
                case R.id.btn_open_photo:
                    choosePick();
                    break;
                case R.id.btn_mode:
                    PopupMenu popupModeMenu = new PopupMenu(MainActivity.this, mBtnMode);
                    popupModeMenu.getMenuInflater().inflate(R.menu.mode_items, popupModeMenu.getMenu());

                    popupModeMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            switch(item.getItemId()) {
                                case R.id.menu_mode_grid:
                                    mEffectView.setMode(Mode.GRID);
                                    break;
                                case R.id.menu_mode_path:
                                    mEffectView.setMode(Mode.PATH);
                                    break;
                                }
                            return true;
                        }
                    });
                    popupModeMenu.show();
                    break;
                case R.id.btn_effect:
                    PopupMenu popupEffectMenu = new PopupMenu(MainActivity.this, mBtnEffect);
                    popupEffectMenu.getMenuInflater().inflate(R.menu.effect_items, popupEffectMenu.getMenu());

                    popupEffectMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            switch(item.getItemId()) {
                                case R.id.menu_effect_grid:
                                    mEffectView.setEffect(Effect.GRID);
                                    break;
                                case R.id.menu_effect_blur:
                                    mEffectView.setEffect(Effect.BLUR);
                                    break;
                                case R.id.menu_effect_color:
                                    mEffectView.setMosaicColor(0xFF4D4D4D);
                                    mEffectView.setEffect(Effect.COLOR);
                                    break;
                            }
                            return true;
                        }
                    });
                    popupEffectMenu.show();
                    break;
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int screenWidth = displayMetrics.widthPixels;
        int screenHeight = displayMetrics.heightPixels;

        Log.v(TAG, "requestCode : " + requestCode + " ,resultCode : " + resultCode + " ,data : " + data);
        switch(requestCode) {
            case REQ_PICK_IMAGE:
                if(data != null) {
                    mEffectView.setImagePath(getFilePath(data) == null? data.getData().getPath() : getFilePath(data), screenWidth, screenHeight);
                }
                break;
            case TAKE_PHOTO:
                mEffectView.setImagePath(mCameraUri.getPath(), screenWidth, screenHeight);
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode) {
            case PermissionUtil.REQUEST_PERMISSION:
            case PermissionUtil.REQUEST_PERMISSION_AGAIN:
                if (permissions == null || permissions.length == 0 || grantResults == null
                        || grantResults.length == 0) {
                    Log.e(TAG, "onRequestPermissionsResult(" + requestCode + "): error size: "
                            + (permissions == null ? "null" : permissions.length));
                    return;
                }
                boolean allGranted = true;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        Log.w(TAG,
                                "onRequestPermissionsResult(" + requestCode + "): User not grant permission" + permissions[i]);
                        allGranted = false;
                    }
                }

                if (allGranted) {
                    android.util.Log.d(TAG, "onRequestPermissionsResult(" + requestCode + "): all permission granted");
                } else {
                    Log.i(TAG,"onRequestPermissionsResult(" + requestCode + "): not grant all permission");
                    if (PermissionUtil.isAllPermissionNeverAskChecked(this, permissions)) {
                        new AlertDialog.Builder(this)
                        .setMessage(R.string.permission_dialog_msg_user_never_ask)
                        .setPositiveButton(R.string.okay_action,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        Uri uri = Uri.fromParts("package",
                                                getPackageName(), null);
                                        intent.setData(uri);
                                        startActivity(intent);
                                    }
                                })
                        .setNegativeButton(R.string.cancel_action,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if(dialog != null) {
                                            dialog.cancel();
                                            dialog.dismiss();
                                        }
                                    }
                                }).setCancelable(false).show()
                        .setCanceledOnTouchOutside(false);
                    }
                }
                break;
        }
    }

}
