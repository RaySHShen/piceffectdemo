package com.fihtdc.demo.utils;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class FileUtil {

    public static void notifyMediaScanner(Context context, File filePath) {
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent.setData(Uri.parse("file://" + filePath.getPath()));
        context.sendBroadcast(intent);
    }

    public static void createFile(File outputFile) {
        try {
            if(outputFile.exists()) {
                outputFile.delete();
            }
            outputFile.createNewFile();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

}
