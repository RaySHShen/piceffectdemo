package com.fihtdc.demo.utils;

import java.util.ArrayList;

import com.fihtdc.demo.R;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

public class PermissionUtil {

    private final static String TAG = "PermissionUtil";

    public static final int REQUEST_PERMISSION = 1;
    public static final int REQUEST_PERMISSION_AGAIN = 2;

    public final static String[] permissionList = new String[] {
        Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public static boolean isAllPermissionGranted(Context context) {
        for (String permission : permissionList) {
            if (!permissionGranted(context, permission)) {
                Log.i(TAG, "isAllPermissionGranted(): " + permission + " not granted");
                return false;
            }
        }
        return true;
    }

    public static String[] needPermissionGrant(Context context) {
        ArrayList<String> list = new ArrayList<String>();
        for (String permission : permissionList) {
            if (!permissionGranted(context, permission)) {
                Log.i(TAG, "isAllPermissionGranted(): " + permission + " not granted");
                list.add(permission);
            }
        }
        return list.toArray(new String[0]);
    }

    public static boolean shouldShowRequestPermissionRationale(Activity activity, String[] list) {
        for (String permission : list) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isAllPermissionNeverAskChecked(Activity activity, String[] list) {
        for (String permission : list) {
            if (!permissionGranted(activity, permission)
                    && ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                return false;
            }
        }
        return true;
    }

    public static boolean permissionGranted(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static void showPermissionDialog(Context context, final Activity activity) {
        final String[] needGrantList = needPermissionGrant(context);
        if (needGrantList.length > 0) {
            if (shouldShowRequestPermissionRationale(activity, needGrantList)) {
                // if user disable or deny the permission before
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(R.string.permission_dialog_msg_ask_permission)
                        .setCancelable(false)
                        .setPositiveButton(R.string.okay_action,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        ActivityCompat.requestPermissions(
                                                activity, needGrantList,
                                                REQUEST_PERMISSION_AGAIN);
                                    }
                                });
                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        if(dialog != null) {
                            dialog.cancel();
                            dialog.dismiss();
                        }
                    }
                });
                builder.show().setCanceledOnTouchOutside(false);

            } else {
                // First time show the request permission
                ActivityCompat.requestPermissions(activity, needGrantList, REQUEST_PERMISSION);
            }
        } else {
            Log.w(TAG, "Already have permission");
        }
    }
}
