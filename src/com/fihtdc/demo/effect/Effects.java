package com.fihtdc.demo.effect;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Bitmap.Config;

import com.fihtdc.demo.utils.BitmapUtil;

public class Effects {

    public static Bitmap getColorMosaic(int imageWidth, int imageHeight, int color) {
        if (imageWidth <= 0 || imageHeight <= 0) {
            return null;
        }

        Bitmap bitmap = Bitmap.createBitmap(imageWidth, imageHeight, Config.ARGB_4444);
        Canvas canvas = new Canvas(bitmap);
        Rect rect = new Rect(0, 0, imageWidth, imageHeight);
        Paint paint = new Paint();
        paint.setColor(color);
        canvas.drawRect(rect, paint);
        canvas.save();
        return bitmap;
    }

    public static Bitmap getBlurMosaic(Bitmap layer, int imageWidth, int imageHeight) {
        if (imageWidth <= 0 || imageHeight <= 0) {
            return null;
        }

        if (layer == null) {
            return null;
        }
        return BitmapUtil.blur(layer);
    }

    public static Bitmap getGridMosaic(Bitmap layer, int imageWidth, int imageHeight, int gridWitgh) {
        if (imageWidth <= 0 || imageHeight <= 0) {
            return null;
        }

        Bitmap bitmap = Bitmap.createBitmap(imageWidth, imageHeight, Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        int horCount = (int) Math.ceil(imageWidth / (float) gridWitgh);
        int verCount = (int) Math.ceil(imageHeight / (float) gridWitgh);

        Paint paint = new Paint();
        paint.setAntiAlias(true);

        for (int horIndex = 0; horIndex < horCount; ++horIndex) {
            for (int verIndex = 0; verIndex < verCount; ++verIndex) {
                int l = gridWitgh * horIndex;
                int t = gridWitgh * verIndex;
                int r = l + gridWitgh;
                if (r > imageWidth) {
                    r = imageWidth;
                }
                int b = t + gridWitgh;
                if (b > imageHeight) {
                    b = imageHeight;
                }
                int color = layer.getPixel(l, t);
                Rect rect = new Rect(l, t, r, b);
                paint.setColor(color);
                canvas.drawRect(rect, paint);
            }
        }
        canvas.save();
        return bitmap;
    }

}
